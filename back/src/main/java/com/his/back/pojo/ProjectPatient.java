package com.his.back.pojo;

public class ProjectPatient extends patient_project{
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProjectPatient{" +
                "price=" + price +
                '}';
    }
}
