package com.his.back.pojo;

public class drug {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_id
     *
     * @mbggenerated
     */
    private Integer drugId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_name
     *
     * @mbggenerated
     */
    private String drugName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_standard
     *
     * @mbggenerated
     */
    private String drugStandard;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_unit
     *
     * @mbggenerated
     */
    private String drugUnit;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_unitPrice
     *
     * @mbggenerated
     */
    private Double drugUnitprice;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_dosageForm
     *
     * @mbggenerated
     */
    private String drugDosageform;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column drug.drug_type
     *
     * @mbggenerated
     */
    private String drugType;

    @Override
    public String toString() {
        return "drug{" +
                "drugId=" + drugId +
                ", drugName='" + drugName + '\'' +
                ", drugStandard='" + drugStandard + '\'' +
                ", drugUnit='" + drugUnit + '\'' +
                ", drugUnitprice=" + drugUnitprice +
                ", drugDosageform='" + drugDosageform + '\'' +
                ", drugType='" + drugType + '\'' +
                '}';
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_id
     *
     * @return the value of drug.drug_id
     *
     * @mbggenerated
     */
    public Integer getDrugId() {
        return drugId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_id
     *
     * @param drugId the value for drug.drug_id
     *
     * @mbggenerated
     */
    public void setDrugId(Integer drugId) {
        this.drugId = drugId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_name
     *
     * @return the value of drug.drug_name
     *
     * @mbggenerated
     */
    public String getDrugName() {
        return drugName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_name
     *
     * @param drugName the value for drug.drug_name
     *
     * @mbggenerated
     */
    public void setDrugName(String drugName) {
        this.drugName = drugName == null ? null : drugName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_standard
     *
     * @return the value of drug.drug_standard
     *
     * @mbggenerated
     */
    public String getDrugStandard() {
        return drugStandard;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_standard
     *
     * @param drugStandard the value for drug.drug_standard
     *
     * @mbggenerated
     */
    public void setDrugStandard(String drugStandard) {
        this.drugStandard = drugStandard == null ? null : drugStandard.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_unit
     *
     * @return the value of drug.drug_unit
     *
     * @mbggenerated
     */
    public String getDrugUnit() {
        return drugUnit;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_unit
     *
     * @param drugUnit the value for drug.drug_unit
     *
     * @mbggenerated
     */
    public void setDrugUnit(String drugUnit) {
        this.drugUnit = drugUnit == null ? null : drugUnit.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_unitPrice
     *
     * @return the value of drug.drug_unitPrice
     *
     * @mbggenerated
     */
    public Double getDrugUnitprice() {
        return drugUnitprice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_unitPrice
     *
     * @param drugUnitprice the value for drug.drug_unitPrice
     *
     * @mbggenerated
     */
    public void setDrugUnitprice(Double drugUnitprice) {
        this.drugUnitprice = drugUnitprice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_dosageForm
     *
     * @return the value of drug.drug_dosageForm
     *
     * @mbggenerated
     */
    public String getDrugDosageform() {
        return drugDosageform;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_dosageForm
     *
     * @param drugDosageform the value for drug.drug_dosageForm
     *
     * @mbggenerated
     */
    public void setDrugDosageform(String drugDosageform) {
        this.drugDosageform = drugDosageform == null ? null : drugDosageform.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column drug.drug_type
     *
     * @return the value of drug.drug_type
     *
     * @mbggenerated
     */
    public String getDrugType() {
        return drugType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column drug.drug_type
     *
     * @param drugType the value for drug.drug_type
     *
     * @mbggenerated
     */
    public void setDrugType(String drugType) {
        this.drugType = drugType == null ? null : drugType.trim();
    }
}