package com.his.back.pojo;

public class registerLevel {
    private int registerLevelId;
    private String registerLevelName;

    public int getRegisterLevelId() {
        return registerLevelId;
    }

    public void setRegisterLevelId(int registerLevelId) {
        this.registerLevelId = registerLevelId;
    }

    public String getRegisterLevelName() {
        return registerLevelName;
    }

    public void setRegisterLevelName(String registerLevelName) {
        this.registerLevelName = registerLevelName;
    }

    @Override
    public String toString() {
        return "register_level{" +
                "registerLevelId=" + registerLevelId +
                ", registerLevelName='" + registerLevelName + '\'' +
                '}';
    }
}
