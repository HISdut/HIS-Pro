package com.his.back.service.serviceImpl;

import com.his.back.dao.*;
import com.his.back.pojo.*;
import com.his.back.service.ReceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceptionServiceImpl implements ReceptionService {

    @Autowired
    private memberMapper mM;
    @Autowired
    private patient_projectMapper ppM;

    @Autowired
    private invoiceMapper iM;
    @Autowired
    private registerLevelMapper rlM;
    @Autowired
    private departmentMapper dM;

    @Override
    public int addMember(member m) {
        return mM.insertSelective(m);
    }

    @Override
    public int registerPatiPro(patient_project pp) {
        return ppM.insertSelective(pp);
    }

    @Override
    public int registerInvoice(invoice inv) {
        return iM.insertSelective(inv);
    }

    @Override
    public member findRegisterMember(String patientId) {
        return mM.findRegisterMember(patientId);
    }

    @Override
    public registerLevel findLevelById(int levelId) {
        return rlM.selectByPrimaryKey(levelId);
    }

    @Override
    public department findRoomById(String roomId) {
        return dM.selectByPrimaryKey(roomId);
    }

    @Override
    public void pushPatient(String patientId) {
        member m=findRegisterMember(patientId);
        m.setStateId(2);
        mM.updateByPrimaryKeySelective(m);
    }
}
