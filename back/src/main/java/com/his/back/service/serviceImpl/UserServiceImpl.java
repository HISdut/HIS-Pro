package com.his.back.service.serviceImpl;

import com.his.back.dao.userMapper;
import com.his.back.pojo.user;
import com.his.back.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private userMapper userMapper;

    @Override
    public user findById(String id){
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insert(user user) {
        return userMapper.insertSelective(user);
    }

    @Override
    public List<user> allUser() {
        return userMapper.allUser();
    }

    @Override
    public int delete(String userId){
        return userMapper.deleteByPrimaryKey(userId);
    }
    @Override
    public int change(user user) {
        return userMapper.updateByPrimaryKeySelective(user);
    }
}
