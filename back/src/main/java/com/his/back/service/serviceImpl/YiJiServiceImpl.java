package com.his.back.service.serviceImpl;

import com.his.back.dao.memberMapper;
import com.his.back.dao.patient_projectMapper;
import com.his.back.pojo.member;
import com.his.back.pojo.patient_project;
import com.his.back.pojo.patient_projectKey;
import com.his.back.pojo.project;
import com.his.back.service.YiJiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YiJiServiceImpl implements YiJiService {
    @Autowired
    memberMapper memberMapper;
    @Autowired
    patient_projectMapper patientProjectMapper;



    @Override
    public List<member> find(String content) {
        return memberMapper.finds(content);
    }

    @Override
    public List<project> findProjC(String patientId) {
        return patientProjectMapper.findProjC(patientId);
    }

    @Override
    public List<project> findProjY(String patientId) {
        return patientProjectMapper.findProjY(patientId);
    }

    @Override
    public List<project> findProjZ(String patientId) {
        return patientProjectMapper.findProjZ(patientId);
    }

    @Override
    public int change(patient_project record) {
        return patientProjectMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public patient_project findProj(String patientId, String projName) {
        patient_projectKey patientProjectKey=new patient_projectKey();
        patientProjectKey.setPatientId(patientId);
        patientProjectKey.setProjectName(projName);
        return patientProjectMapper.selectByPrimaryKey(patientProjectKey);
    }

    @Override
    public int delete(patient_project record) {
        patient_projectKey patientProjectKey=new patient_projectKey();
        patientProjectKey.setPatientId(record.getPatientId());
        patientProjectKey.setProjectName(record.getProjectName());
        return patientProjectMapper.deleteByPrimaryKey(patientProjectKey);
    }


}
