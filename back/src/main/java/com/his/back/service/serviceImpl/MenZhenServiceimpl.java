package com.his.back.service.serviceImpl;

import com.his.back.service.MenZhenService;
import com.his.back.dao.memberMapper;
import com.his.back.pojo.member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenZhenServiceimpl implements MenZhenService{
    @Autowired
    memberMapper memberMapper;

    @Override
    public member find(String content){
        return memberMapper.find(content);
    }

}
