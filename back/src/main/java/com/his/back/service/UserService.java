package com.his.back.service;

import com.his.back.pojo.user;

import java.util.List;

public interface UserService {
    user findById(String id);
    int insert(user user);
    List<user> allUser();
    int delete(String userId);
    int change(user user);
}
