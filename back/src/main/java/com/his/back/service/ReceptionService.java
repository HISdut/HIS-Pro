package com.his.back.service;

import com.his.back.pojo.*;
import org.springframework.stereotype.Service;

@Service
public interface ReceptionService {
    public int addMember(member m);

    public int registerPatiPro( patient_project pp);

    int registerInvoice(invoice inv);

    member findRegisterMember(String patientId);

    registerLevel findLevelById(int levelId);

    department findRoomById(String roomId);

    void pushPatient(String patientId);
}
