package com.his.back.service;

import com.his.back.pojo.drug;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DrugService {
    List<drug> search(String query);
    int add(drug record);
    int change(drug record);
    int delete(Integer record);
}
