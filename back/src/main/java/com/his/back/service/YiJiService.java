package com.his.back.service;

import com.his.back.pojo.member;
import com.his.back.pojo.patient_project;
import com.his.back.pojo.project;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface YiJiService {
    /**
     * 查找患者,模糊查询
     * @return member
     */
    List<member> find(String content);

    /**
     * 查询患者需要执行的检查项目
     * @param patientId
     * @return
     */
    List<project> findProjC(String patientId);

    /**
     * 查询患者需要执行的检查验项目
     * @param patientId
     * @return
     */
    List<project> findProjY(String patientId);

    /**
     * 查询患者需要执行的处置项目
     * @param patientId
     * @return
     */
    List<project> findProjZ(String patientId);

    /**
     * 更改状态
     * @param record
     * @return int
     */
    int change(patient_project record);

    /**
     * 查找记录
     * @param patientId,projName
     * @return patient_project
     */
    patient_project findProj(String patientId,String projName);

    /**
     * 取消执行，删除记录
     * @param record
     * @return int
     */
    int delete(patient_project record);
}
