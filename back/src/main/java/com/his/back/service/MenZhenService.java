package com.his.back.service;

import com.his.back.pojo.member;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MenZhenService {
    /**
     * 通过患者的姓名或者病历号查找患者
     * @param content
     * @return
     */
    member find(String content);
}
