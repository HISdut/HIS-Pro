package com.his.back.service.serviceImpl;

import com.his.back.dao.drugMapper;
import com.his.back.pojo.drug;
import com.his.back.service.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugServiceImpl implements DrugService {
    @Autowired
    drugMapper drugMapper;

    @Override
    public List<drug> search(String query) {
        return drugMapper.search(query);
    }

    @Override
    public int add(drug record) {
        return drugMapper.insertSelective(record);
    }

    @Override
    public int change(drug record) {
        return drugMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int delete(Integer record) {
        return drugMapper.deleteByPrimaryKey(record);
    }
}
