package com.his.back.controller;

import com.his.back.pojo.user;
import com.his.back.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@CrossOrigin(origins = "*",maxAge = 3600)
@RestController
@RequestMapping("/user")
public class LoginController {
    @Resource
    private UserService userService;

    @RequestMapping("/login")
    public user login(@RequestParam String userId){
        System.out.println("login ->"+userService.findById(userId));
        return userService.findById(userId);
    }
    @RequestMapping("/allUser")
    public List<user> allUser() {
        // System.out.println("find all user->" + userService.allUser().toString());
        return userService.allUser();
    }
     @RequestMapping(value="/insert",method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public int insert(@RequestBody user user)
    {
        return userService.insert(user);
    }
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    @ResponseBody
    public int delete(@RequestParam("userId") String userId){
        System.out.println("delete"+userId);
    return userService.delete(userId);
    }

    @RequestMapping(value = "/change",method = RequestMethod.POST,consumes = "application/json")
    public int change(@RequestBody  user user){
        System.out.println("change"+user.toString());
   return userService.change(user);
    }

}
