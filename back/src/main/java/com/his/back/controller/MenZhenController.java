package com.his.back.controller;

import com.his.back.service.MenZhenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.his.back.pojo.member;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/menZhen")
public class MenZhenController {
    @Autowired
    MenZhenService menZhenService;

    @RequestMapping("/find")
    public member find(@RequestParam String content){
        System.out.println("find ->content: "+content);
        System.out.println("-> member: "+menZhenService.find("%"+content+"%").toString());
        return menZhenService.find("%"+content+"%");
    }
}
