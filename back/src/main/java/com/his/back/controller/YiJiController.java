package com.his.back.controller;

import com.his.back.pojo.member;
import com.his.back.pojo.patient_project;
import com.his.back.pojo.project;
import com.his.back.service.YiJiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/yiJi")
public class YiJiController {
    @Autowired
    YiJiService yiJiService;

    @RequestMapping("/find")
    public List<member> find(@RequestParam String content){
        System.out.println("find ->content: "+content);
        System.out.println("-> member: "+yiJiService.find("%"+content+"%").toString());
        return yiJiService.find("%"+content+"%");
    }

    @RequestMapping("/findProjC")
    public List<project> findProjC(@RequestParam String patientId){
        System.out.println("findProjectList ->id: "+patientId);
        System.out.println("-> list: "+yiJiService.findProjC(patientId));
        return yiJiService.findProjC(patientId);
    }
    @RequestMapping("/findProjY")
    public List<project> findProjY(@RequestParam String patientId){
        System.out.println("findProjectList ->id: "+patientId);
        System.out.println("-> list: "+yiJiService.findProjY(patientId));
        return yiJiService.findProjY(patientId);
    }
    @RequestMapping("/findProjZ")
    public List<project> findProjZ(@RequestParam String patientId){
        System.out.println("findProjectList ->id: "+patientId);
        System.out.println("-> list: "+yiJiService.findProjZ(patientId));
        return yiJiService.findProjZ(patientId);
    }

    @PutMapping("/enter")
    public String enter(@RequestParam String patientId,@RequestParam String projName){
        System.out.println("enter-> "+patientId+projName);
        if(yiJiService.findProj(patientId,projName)==null){
            return "-1";
        }else {
            patient_project result=yiJiService.findProj(patientId,projName);
            System.out.println("result"+result.toString());
            result.setStateId(6);
            if(yiJiService.change(result)==0){
                return "0";
            }else {
                return "1";
            }
        }
    }


    @DeleteMapping("/unEnter")
    public String unEnter(@RequestParam String patientId,@RequestParam String projName){
        System.out.println("unEnter-> "+patientId+projName);
        if(yiJiService.findProj(patientId,projName)==null){
            return "-1";
        }else {
            patient_project result=yiJiService.findProj(patientId,projName);
            System.out.println("result"+result.toString());
            if(yiJiService.delete(result)== 0){
                return "0";
            }else {
                return "1";
            }
        }
    }

    @PutMapping("/result")
    public String result(@RequestParam String patientId,@RequestParam String projName,@RequestParam String result){
        System.out.println("result-> "+patientId+projName+result);
        if(yiJiService.findProj(patientId,projName)== null){
            return "-1";
        }else {
            patient_project findResult=yiJiService.findProj(patientId,projName);
            System.out.println("result"+findResult.toString());
            findResult.setStateId(7);
            findResult.setResult(result);
            if(yiJiService.change(findResult)== 0){
                return "0";
            }else {
                return "1";
            }
        }
    }
}
