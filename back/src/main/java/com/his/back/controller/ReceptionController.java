package com.his.back.controller;

import com.his.back.pojo.*;
import com.his.back.service.ReceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/reception")
public class ReceptionController {
    @Autowired
    private ReceptionService receptionService;
    @RequestMapping("/addMember")
    public int addMember(@RequestBody member member){


        System.out.println("add member :"+member);
        return receptionService.addMember(member);
    }

    @RequestMapping("/registerPatiPro")
    public int registerPatiPro(@RequestBody patient_project pp){
        System.out.println("register PatiPro :"+pp);
        return receptionService.registerPatiPro(pp);
    }

    @RequestMapping("/registerInvoice")
    public int registerInvoice(@RequestBody invoice inv){
        System.out.println("register invoice :"+inv);
        return receptionService.registerInvoice(inv);
    }

    @RequestMapping("/findPatient")
    public Map<String,Object> findExMemberById(@RequestParam String patientId){
        //在memberd的基础上附加看诊科室,挂号级别
       Map<String,Object> map=new HashMap();
        member m=receptionService.findRegisterMember(patientId);
        int levelId=m.getRegisterLeverId();
        registerLevel level=receptionService.findLevelById(levelId);
        String roomId = m.getRoomId();
        department department=receptionService.findRoomById(roomId);
        map.put("member",m);
        map.put("room",department.getDeptName());
        map.put("registerLevel",level.getRegisterLevelName());
        map.put("state","未看诊");
        return map;
    }
    @RequestMapping("/pushPatient")
    public void pushPatient(@RequestParam String patientId){
        //退号即改变member的stateId;
        receptionService.pushPatient(patientId);
    }
    /*@RequestMapping("/findPatiPro")
    public List<patient_project>*/
}
