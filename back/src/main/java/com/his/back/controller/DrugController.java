package com.his.back.controller;

import com.his.back.pojo.drug;
import com.his.back.service.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/yaofang")
public class DrugController {
    @Autowired
    DrugService drugService;

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public List<drug> search(@RequestParam String query){
        System.out.println(query);
        if(drugService.search("%"+query+"%")==null) {
            return null;
        }else {
            System.out.println("search:"+drugService.search("%"+query+"%").toString());
            return drugService.search("%"+query+"%");
        }
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String add(@RequestBody drug drug){
        System.out.println("add"+drug.toString());
        if(drugService.add(drug)==1){
            return "true";
        }else {
            return "false";
        }
    }

    @RequestMapping(value = "/change",method = RequestMethod.POST)
    public String change(@RequestBody drug drug){
        System.out.println("change"+drug.toString());
        if(drugService.change(drug)==1){
            return "true";
        }else{
            return "false";
        }
    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public String delete(@RequestParam String id){
        System.out.println("delete"+id);
        Integer drugId=Integer.valueOf(id);
        if(drugService.delete(drugId)==1){
            return "true";
        }else {
            return "false";
        }
    }
}
