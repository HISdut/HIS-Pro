package com.his.back.dao;

import com.his.back.pojo.registerLevel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component("registerLevelMapper")
public interface registerLevelMapper {
    registerLevel selectByPrimaryKey(Integer register_level_id);
}
