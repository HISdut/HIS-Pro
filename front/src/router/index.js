import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/views/HelloWorld'
import Yiji from '@/components/views/Yiji'
import Drug from '@/components/views/Drug'
import MenZhen from '@/components/views/MenZhen'
import InitMain from '../components/views/reception/InitMain'
import GetANum from '@/components/views/reception/GetANum'
import ReturnANum from '@/components/views/reception/ReturnANum'
import GetMoney from '@/components/views/reception/GetMoney'
import ReturnMoney from '@/components/views/reception/ReturnMoney'
import ReInvoice from '@/components/views/reception/ReInvoice'
import Invoice from '@/components/views/reception/Invoice'
import CostQuery from '@/components/views/reception/CostQuery'
import UserCheck from '@/components/views/reception/UserCheck'
import Admin from '@/components/views/Admin'
import UserAdmin from '@/components/views/reception/UserAdmin'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/admin/',
      name: 'Admin',
      component: Admin
    },
    {
      path: '/yiji/',
      name: 'Yiji',
      component: Yiji
    },
    {
      path: '/yaofang/',
      name: 'Drug',
      component: Drug
    },
    {
      path: '/menzhen/',
      name: 'MenZhen',
      component: MenZhen
    },
    {
      path:'/initMain',//url path
      name:'InitMain',
      component:InitMain
    },
    {
      path:'/getANum',//url path
      name:'GetANum',
      component:GetANum
    },
    {
      path:'/returnANum',//url path
      name:'ReturnANum',
      component:ReturnANum
    },
    {
      path:'/getMoney',//url path
      name:'GetMoney',
      component:GetMoney
    },
    {
      path:'/returnMoney',//url path
      name:'ReturnMoney',
      component:ReturnMoney
    },
    {
      path:'/reInvoice',//url path
      name:'ReInvoice',
      component:ReInvoice
    },
    {
      path:'/invoice',//url path
      name:'Invoice',
      component:Invoice
    },
    {
      path:'/costQuery',//url path
      name:'CostQuery',
      component:CostQuery
    },
    {
      path:'/userCheck',//url path
      name:'UserCheck',
      component:UserCheck
    },
    {
      path:'/userAdmin',//url path
      name:'UserAdmin',
      component:UserAdmin
    },
    
  ]
})
